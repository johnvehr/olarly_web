import React from "react";
import {Col, Container, Row} from 'reactstrap'
import Navigation from '../shared/navigation'

class LandingLayout extends React.Component {
  render() {
    return (
      <div>
        <Navigation />
      </div>
    )
  }
}

export default LandingLayout
